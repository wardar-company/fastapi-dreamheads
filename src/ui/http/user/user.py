from fastapi import APIRouter, status, Response
from src.user.repository.user_repository import UserRepository, NotFoundError
from src.app.value_object.id_value_object import Id
from src.app.value_object.name_value_object import Name
from src.app.value_object.description_value_object import Description
from src.user.model.user.user import User
from pydantic import BaseModel
from typing import Optional
from src.app.value_object.email_value_object import Email

router = APIRouter()
userRepository = UserRepository()


class UserDto(BaseModel):
    name: str
    description: Optional[str] = None
    email: str


@router.post("/user", status_code=status.HTTP_201_CREATED)
def create(id: int,userDto: UserDto):
    user = userRepository.add(
        User(Id(id), Name(userDto.name),
             None if userDto.description is None else Description(userDto.description), Email(userDto.email)))

    return {
        'id': user.id.value,
        'name': user.name.value,
        'description': None if user.description is None else user.description.value,
        'email': user.email.value
    }


@router.get("/user")
def index() -> list:
    users = userRepository.list()
    return list(
        map(
            lambda user:
            {
                'id': user.id.value,
                'name': user.name.value,
                'description': None if user.description is None else user.description.value,
                'email': user.email.value
            }, users
        )
    )


@router.get("/user/{id}")
def view(id: int):
    user = userRepository.get(Id(id))
    return {
        'id': user.id.value,
        'name': user.name.value,
        'description': None if user.description is None else user.description.value,
        'email': user.email.value
    }


@router.patch("/user/{id}")
def update(id: int, userDto: UserDto):
    user = userRepository.get(Id(id))
    if userDto.name is not None:
        user.name = Name(userDto.name)
    user.description = None if userDto.description is None else Description(userDto.description)
    user.email = Email(userDto.email)
    userRepository.update()
    return {
        'id': user.id.value,
        'name': user.name.value,
        'description': None if user.description is None else user.description.value,
        'email': user.email.value
    }


@router.delete("/user/{id}", status_code=status.HTTP_204_NO_CONTENT)
def delete(id: int):
    try:
        userRepository.destroy_by_id(Id(id))
    except NotFoundError:
        return Response(status_code=status.HTTP_404_NOT_FOUND)
    else:
        return Response(status_code=status.HTTP_204_NO_CONTENT)





