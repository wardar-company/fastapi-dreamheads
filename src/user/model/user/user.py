from typing import Optional
import datetime
import sqlalchemy
from sqlalchemy import Column
from src.app.value_object.sqlalchemy.id_decorator import IdDecorator
from src.app.value_object.sqlalchemy.name_decorator import NameDecorator
from src.app.value_object.sqlalchemy.description_decorator import DescriptionDecorator
from src.app.value_object.sqlalchemy.email_decorator import EmailDecorator
from src.app.value_object.id_value_object import Id
from src.app.value_object.name_value_object import Name
from src.app.value_object.description_value_object import Description
from src.config.db import Base
from src.app.value_object.email_value_object import Email


class User(Base):
    __tablename__ = 'user'
    id = Column(IdDecorator, primary_key=True, index=True)
    name = Column(NameDecorator)
    description = Column(DescriptionDecorator)
    email = Column(EmailDecorator,  unique=True)



    def __init__(self, id: Id, name: Name,
                 description: Optional[Description], email: Email):
        self.id = id
        self.name = name
        self.description = description
        self.email = email


    def get_id(self) -> Id:
        return self.id

    def get_name(self) -> Name:
        return self.name

    def get_description(self) -> Optional[Description]:
        return self.description

    def get_email(self) -> Email:
        return self.email
