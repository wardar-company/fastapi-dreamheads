from typing import List
from src.user.model.user.user import User
from src.app.repository.abstract_repository import AbstractRepository
from src.app.value_object.id_value_object import Id
from src.app.value_object.email_value_object import Email


class UserRepository(AbstractRepository):
    def __init__(self):
        super().__init__()

    def add(self, user: User) -> User:
        self.session.add(user)
        self.session.flush()
        self.session.commit()
        return user

    def get(self, model_id: Id) -> User:
        user = self.session.query(User).get(model_id)
        if not user:
            raise UserNotFoundError(model_id)
        return user


    def list(self) -> List[User]:
        return self.session.query(User).all()

    def update(self):
        self.session.flush()
        self.session.commit()

    def get_by_email(self, email: Email) -> User:
        return self.session.query(User).get(email)

    def destroy_by_id(self, model_id: Id) -> None:
        del_id: User = self.session.query(User).filter(User.id == model_id).first()
        if not del_id:
            raise UserNotFoundError(model_id)
        self.session.delete(del_id)
        self.session.commit()






class NotFoundError(Exception):
    name : str
    def __init__(self, model_id:Id):
        super().__init__(f'{self.name} not found, id: {model_id}')



class UserNotFoundError(NotFoundError):
    name: str = 'User'