from sqlalchemy.types import TypeDecorator, VARCHAR

from src.app.value_object.email_value_object import Email


class EmailDecorator(TypeDecorator):
    impl = VARCHAR

    def process_bind_param(self, value_object: Email, dialect) -> str:
        if value_object is not None:
            return value_object.get_value()

    def process_result_value(self, value: str, dialect) -> Email:
        if value is not None:
            return Email(value)
