from fastapi import FastAPI
from src.config.settings import settings
from src.config.routes import api_router



app = FastAPI(title=settings.PROJECT_NAME, openapi_url="/api/v1/openapi.json")
app.include_router(api_router, prefix=settings.API_V1_STR)

