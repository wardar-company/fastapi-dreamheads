from pydantic import BaseSettings
import os

class Settings(BaseSettings):
    API_V1_STR: str = "/api/v1"
    PROJECT_NAME: str = "Siren"
    POSTGRESQL_USER: str = "fast"
    POSTGRESQL_PASS: str = "password"
    POSTGRESQL_HOST: str = "database"
    POSTGRESQL_PORT: str = "5432"
    POSTGRESQL_DB: str = "siren"


settings = Settings()
